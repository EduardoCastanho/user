## This repository, keeps my Sublime-Text-3 work configurations and packages.##
## If you pretend to use then please use the following instructions. ## 
# How to "install": #

0- Close Sublime-Text.

1- Go to Sublime-Text packages config folder:

```
#!bash
$ cd ~/.config/sublime-text-3/Packages

```
2- Clone the repository:

```
#!bash
$ git clone https://YOUR_BITBUCKET_USERNAME@bitbucket.org/SwitchPT/User.git

```
3- Open Sublime-Text; 

4- Install [Package Control](https://packagecontrol.io/installation); 

5- Wait for the installation process reach the end; 

6- Restart Sublime-Text;  

7- Done!!!